<?php

namespace backend\controllers\actions;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;

class IndexAction extends Action {
    public string $searchModel;

    /**
     * @throws NotInstantiableException
     * @throws InvalidConfigException
     */
    public function run(): string {
        $searchModel = Yii::$container->get($this->searchModel);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->controller->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}