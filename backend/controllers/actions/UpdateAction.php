<?php

namespace backend\controllers\actions;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;
use yii\web\Response;

class UpdateAction extends Action {
    public string $model;

    /**
     * @throws NotInstantiableException
     * @throws InvalidConfigException
     */
    public function run($id): string|Response {
        $model = Yii::$container->get($this->model);
        $result = $model->findModel($id);
        $isLoaded = $result?->load(Yii::$app->request->post());

        if ($isLoaded && $result?->save()) {
            return $this->controller->redirect(['view', 'id' => $result?->id]);
        }

        return $this->controller->render('update', [
            'model' => $result,
        ]);
    }
}