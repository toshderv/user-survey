<?php

namespace backend\controllers\actions;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;

class ViewAction extends Action {
    public string $model;

    /**
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function run($id): ?string {
        $model = Yii::$container->get($this->model);

        return $this->controller->render('view', [
            'model' => $model->findModel($id),
        ]);
    }
}