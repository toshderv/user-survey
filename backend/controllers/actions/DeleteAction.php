<?php

namespace backend\controllers\actions;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;
use yii\web\Response;

class DeleteAction extends Action {
    public string $model;

    /**
     * @throws NotInstantiableException
     * @throws InvalidConfigException
     */
    public function run($id): Response {
        $model = Yii::$container->get($this->model);
        $model->findModel($id)?->softDelete();

        return $this->controller->redirect(['index']);
    }
}