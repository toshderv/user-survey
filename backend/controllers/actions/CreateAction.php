<?php

namespace backend\controllers\actions;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;
use yii\web\Response;

class CreateAction extends Action {
    public string $model;

    /**
     * @throws NotInstantiableException
     * @throws InvalidConfigException
     */
    public function run(): string|Response {
        $model = Yii::$container->get($this->model);
        $isLoaded = $model->load(Yii::$app->request->post());

        if ($isLoaded && $model->save()) {
            return $this->controller->redirect(['view', 'id' => $model->id]);
        }

        return $this->controller->render('create', [
            'model' => $model,
        ]);
    }
}