<?php

namespace backend\controllers;

use Yii;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ErrorAction;
use yii\web\HttpException;
use yii\web\Response;

use common\models\LoginForm;

class SiteController extends Controller {
    public function behaviors(): array {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'logout'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return Yii::$app->user->identity->isAdmin();
                        },
                        'denyCallback' => function () {
                            throw new HttpException(403, 'You are not authorized to perform this action');
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions(): array {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    public function actionIndex(): string {
        return $this->render('index');
    }

    /**
     * @throws NotInstantiableException
     * @throws InvalidConfigException
     */
    public function actionLogin(): Response|string|null {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'blank';

        $model = Yii::$container->get(LoginForm::class);
        $isLoaded = $model->load(Yii::$app->request->post());

        if ($isLoaded && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout(): Response {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
