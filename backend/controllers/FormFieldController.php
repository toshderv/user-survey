<?php

namespace backend\controllers;

use Yii;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\ViewAction;
use common\models\Field;
use common\models\Form;
use common\models\FormField;
use common\models\FormFieldSearch;

/**
 * FormFieldController implements the CRUD actions for FormField model.
 */
final class FormFieldController extends Controller {
    public function actions(): array {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModel' => FormFieldSearch::class,
            ],
            'view' => [
                'class' => ViewAction::class,
                'model' => FormField::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'model' => FormField::class,
            ],

        ];
    }

    /**
     * @throws NotInstantiableException
     * @throws InvalidConfigException
     */
    public function actionCreate(): string|Response {
        $model = Yii::$container->get(FormField::class);
        $isLoaded = $model->load(Yii::$app->request->post());

        if ($isLoaded && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $modelForms = $this->prepareFormDropDownList();
        $modelFields = $this->prepareFieldDropDownList();

        return $this->render('create', [
            'model' => $model,
            'modelForms' => $modelForms,
            'modelFields' => $modelFields,
        ]);
    }

    /**
     * @throws NotInstantiableException
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionUpdate(int $id): string|Response {
        $model = Yii::$container->get(FormField::class);
        $result = $model->findModel($id);
        $isLoaded = $result?->load(Yii::$app->request->post());

        if ($isLoaded && $result?->save()) {
            return $this->redirect(['view', 'id' => $result?->id]);
        }

        $modelForms = $this->prepareFormDropDownList();
        $modelFields = $this->prepareFieldDropDownList();

        return $this->render('update', [
            'model' => $result,
            'modelForms' => $modelForms,
            'modelFields' => $modelFields,
        ]);
    }

    /**
     * Prepare a dropdown for the Form entity
     */
    private function prepareFormDropDownList(): array {
        $modelForms = Form::find()->all();
        $formItems = ArrayHelper::map($modelForms, 'id', 'name');
        $formParams = [
            'prompt' => 'Select form ...',
        ];

        return [
            'formItems' => $formItems,
            'formParams' => $formParams,
        ];
    }

    /**
     * Prepare a dropdown for the Form entity
     */
    private function prepareFieldDropDownList(): array {
        $modelFields = Field::find()->all();
        $fieldItems = ArrayHelper::map($modelFields, 'id', 'display_name');
        $fieldParams = [
            'prompt' => 'Select field ...',
        ];

        return [
            'fieldItems' => $fieldItems,
            'fieldParams' => $fieldParams,
        ];
    }
}
