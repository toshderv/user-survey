<?php

namespace backend\controllers;

use backend\controllers\actions\CreateAction;
use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\UpdateAction;
use backend\controllers\actions\ViewAction;
use common\models\Field;
use common\models\FieldSearch;

/**
 * FieldController implements the CRUD actions for Field model.
 */
final class FieldController extends AdminController {
    public function actions(): array {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModel' => FieldSearch::class
            ],
            'view' => [
                'class' => ViewAction::class,
                'model' => Field::class
            ],
            'create' => [
                'class' => CreateAction::class,
                'model' => Field::class
            ],
            'update' => [
                'class' => UpdateAction::class,
                'model' => Field::class
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'model' => Field::class
            ],

        ];
    }
}
