<?php

namespace backend\controllers;

use yii\web\Controller;

use backend\controllers\actions\CreateAction;
use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\UpdateAction;
use backend\controllers\actions\ViewAction;
use common\models\Form;
use common\models\FormSearch;

/**
 * FormController implements the CRUD actions for Form model.
 */
final class FormController extends Controller {
    public function actions(): array {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModel' => FormSearch::class
            ],
            'view' => [
                'class' => ViewAction::class,
                'model' => Form::class
            ],
            'create' => [
                'class' => CreateAction::class,
                'model' => Form::class
            ],
            'update' => [
                'class' => UpdateAction::class,
                'model' => Form::class
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'model' => Form::class
            ],

        ];
    }
}
