<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FormField */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Form Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="form-field-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'field_id',
            'form_id',
            'display_name',
            'sort',
            ['attribute' => 'created_at', 'format' => ['datetime', 'php:d-m-Y H:i:s']],
            ['attribute' => 'updated_at', 'format' => ['datetime', 'php:d-m-Y H:i:s']],
        ],
    ]) ?>

</div>
