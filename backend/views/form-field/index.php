<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FormFieldSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Form Fields';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-field-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Form Field', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'display_name',
            'form_id',
            'field_id',
            'sort',
            ['attribute' => 'created_at', 'format' => ['datetime', 'php:d-m-Y H:i:s']],
            ['attribute' => 'updated_at', 'format' => ['datetime', 'php:d-m-Y H:i:s']],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
