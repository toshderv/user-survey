<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\FormField */
/* @var $modelForms common\models\Form Find all */
/* @var $modelFields common\models\Field Find all */
?>

<div class="form-field-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'form_id')
             ->dropDownList($modelForms['formItems'], $modelForms['formParams']) ?>

    <?= $form->field($model, 'field_id')
             ->dropDownList($modelFields['fieldItems'], $modelFields['fieldParams']) ?>

    <?= $form->field($model, 'display_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sort')->textInput(['type' => 'number', 'min' => 1]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
