<?php
return [
    [
        'username' => 'admin',
        'auth_key' => 'o8cGc7GjPbYf8vss-SRmg2UElutPz3jl',
        // adminadmin
        'password_hash' => '$2y$13$nAUNVezqK4U4K0K2D2OW3.3znC7E4mMYMrt9EmnMxhzteqZvDYqFy',
        'password_reset_token' => null,
        'email' => 'admin@admin.loc',
        'status' => '10',
        'created_at' => '1625687903',
        'updated_at' => '1625687903',
        'verification_token' => 'C3yB4iqPpdxxTtG5XzGaLl77k7FjbMdn_1625687903',
        'role' => '1',
        'first_name' => 'Firstadmin',
        'last_name' => 'Lastadmin',
    ],
];
