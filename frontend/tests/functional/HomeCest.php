<?php

namespace frontend\tests\functional;

use frontend\tests\FunctionalTester;

class HomeCest {
    public function checkOpen(FunctionalTester $I) {
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->see('My Application');
        $I->seeLink('About');
        $I->click('About');

        $I->see('This is the About page.');
        $I->seeLink('Contact');
        $I->click('Contact');

        $I->see('If you have business inquiries or other questions, please fill out the following form to contact us.');
        $I->seeLink('Signup');
        $I->click('Signup');

        $I->see('Please fill out the following fields to signup:');
        $I->seeLink('Login');
        $I->click('Login');

        $I->see('Please fill out the following fields to login:');
    }
}