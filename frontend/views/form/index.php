<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FormSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Forms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'user_id',
            ['attribute' => 'updated_at', 'format' => ['datetime', 'php:d-m-Y H:i:s']],

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'user-form-result',
                'template' => '{create}',
                'buttons' => [
                    'create' => function ($url) {
                        return Html::a('Take a survey', $url);
                    },
                ],
            ],
        ],
    ]); ?>

</div>
