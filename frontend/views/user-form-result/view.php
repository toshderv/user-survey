<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserFormResult */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Form Results', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-form-result-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'form_id',
            'user_id',
            [
                'attribute' => 'value',
                'value' => function ($model) {
                    return json_encode($model->value, true);
                },
            ],
            ['attribute' => 'created_at', 'format' => ['datetime', 'php:d-m-Y H:i:s']],
            ['attribute' => 'updated_at', 'format' => ['datetime', 'php:d-m-Y H:i:s']],
            'isDeleted',
        ],
    ]) ?>

</div>
