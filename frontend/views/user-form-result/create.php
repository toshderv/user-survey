<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserFormResult */
/* @var $modelForm common\models\Form */
/* @var $modelFields common\models\Field */

$this->title = 'Create User Form Result';
$this->params['breadcrumbs'][] = ['label' => 'User Form Results', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-form-result-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelForm' => $modelForm,
        'modelFields' => $modelFields,
    ]) ?>

</div>
