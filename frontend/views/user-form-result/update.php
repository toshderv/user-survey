<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserFormResult */
/* @var $modelForm common\models\Form */
/* @var $modelFields common\models\Field */

$this->title = 'Update User Form Result: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Form Results', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-form-result-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelForm' => $modelForm,
        'modelFields' => $modelFields,
    ]) ?>

</div>
