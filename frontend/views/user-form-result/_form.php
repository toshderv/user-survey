<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Field;

/* @var $this yii\web\View */
/* @var $model common\models\UserFormResult */
/* @var $modelForm common\models\Form */
/* @var $modelFields common\models\Field */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form-result-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'form_id')
             ->label(false)
             ->hiddenInput(['value' => $modelForm->id]) ?>

    <?= $form->field($model, 'user_id')
             ->label(false)
             ->hiddenInput(['value' => Yii::$app->user->identity->getId()]) ?>

    <?php foreach ($modelFields as $modelField) {
        $field = $modelField->field;

        echo match ($field->type) {
            Field::FIELD_SELECT => $form->field($model, "value[$field->name]")
                                        ->label($field->display_name)
                                        ->dropDownList([
                                            1 => 'Yes',
                                            0 => 'No',
                                        ], ['prompt' => 'Select ...']),
            Field::FIELD_CHECKBOX => $form->field($model, "value[$field->name]")
                                          ->checkbox(['label' => $field->display_name]),
            Field::FIELD_DATE => $form->field($model, "value[$field->name]")
                                      ->label($field->display_name)
                                      ->textInput(['placeholder' => 'E.g. dd-mm-YYYY']),
            default => $form->field($model, "value[$field->name]")
                            ->label($field->display_name)
                            ->textInput()
        };
    } ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
