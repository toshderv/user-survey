<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserFormResultSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Form Results';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-form-result-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'form_id',
            'user_id',
            [
                'attribute' => 'value',
                'value' => function ($model) {
                    return json_encode($model->value);
                },
            ],
            ['attribute' => 'created_at', 'format' => ['datetime', 'php:d-m-Y H:i:s']],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
