<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Form;
use common\models\UserFormResult;
use common\models\UserFormResultSearch;

/**
 * UserFormResultController implements the CRUD actions for UserFormResult model.
 */
class UserFormResultController extends Controller {
    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserFormResult models.
     *
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = Yii::$container->get(UserFormResultSearch::class);
        $dataProvider = $searchModel->search(
            Yii::$app->request->queryParams,
            Yii::$app->user->identity?->getId()
        );

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserFormResult model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserFormResult model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer|null $id Form ID
     *
     * @return mixed
     */
    public function actionCreate(int $id = null): mixed {
        $model = Yii::$container->get(UserFormResult::class);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $formID = $id;
        $modelForm = Form::findOne($formID);
        $modelFields = $modelForm?->getFormFields()->all();

        return $this->render('create', [
            'model' => $model,
            'modelForm' => $modelForm,
            'modelFields' => $modelFields,
        ]);
    }

    /**
     * Updates an existing UserFormResult model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $formID = $model->form_id;
        $modelForm = Form::findOne($formID);
        $modelFields = $modelForm?->getFormFields()->all();

        return $this->render('update', [
            'model' => $model,
            'modelForm' => $modelForm,
            'modelFields' => $modelFields,
        ]);
    }

    /**
     * Deletes an existing UserFormResult model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->softDelete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserFormResult model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return UserFormResult the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = UserFormResult::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
