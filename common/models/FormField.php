<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "form_fields".
 *
 * @property int $id
 * @property int|null $field_id
 * @property int|null $form_id
 * @property string $display_name
 * @property int $sort sequential number on the form
 * @property int $created_at
 * @property int $updated_at
 * @property boolean $isDeleted
 *
 * @property Field $field
 * @property Form $form
 */
class FormField extends ActiveRecord {
    public static function tableName(): string {
        return 'form_fields';
    }

    public function behaviors(): array {
        return [
            TimestampBehavior::class,
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'isDeleted' => true,
                ],
            ],
        ];
    }

    public function rules(): array {
        return [
            [['field_id', 'form_id', 'sort'], 'integer'],
            [['field_id', 'form_id', 'display_name'], 'required'],
            [['display_name'], 'string', 'max' => 1000],
            [
                ['field_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Field::class,
                'targetAttribute' => ['field_id' => 'id'],
            ],
            [
                ['form_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Form::class,
                'targetAttribute' => ['form_id' => 'id'],
            ],
        ];
    }

    public function attributeLabels(): array {
        return [
            'id' => 'ID',
            'field_id' => 'Field ID',
            'form_id' => 'Form ID',
            'display_name' => 'Display Name',
            'sort' => 'Sort',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Field]].
     */
    public function getField(): ActiveQuery {
        return $this->hasOne(Field::class, ['id' => 'field_id']);
    }

    /**
     * Gets query for [[Form]].
     */
    public function getForm(): ActiveQuery {
        return $this->hasOne(Form::class, ['id' => 'form_id']);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function findModel(int $id): ?FormField {
        if (($model = self::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
