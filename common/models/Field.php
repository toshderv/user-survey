<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "fields".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $display_name title
 * @property string $name name in the db
 * @property int $type 1 - text; 2 - select; 3 - checkbox; 4 - date
 * @property int $created_at
 * @property int $updated_at
 * @property boolean $isDeleted
 *
 * @property User $user
 * @property FormField[] $formFields
 * @method softDelete()
 */
class Field extends ActiveRecord {
    public const FIELD_TEXT = 1;
    public const FIELD_SELECT = 2;
    public const FIELD_CHECKBOX = 3;
    public const FIELD_DATE = 4;

    public static function tableName(): string {
        return 'fields';
    }

    public function behaviors(): array {
        return [
            TimestampBehavior::class,
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'isDeleted' => true,
                ],
            ],
        ];
    }

    public function rules(): array {
        return [
            [['user_id', 'type'], 'integer'],
            [['display_name', 'name', 'type'], 'required'],
            [['display_name', 'name'], 'string', 'max' => 255],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
            [
                'type',
                'in',
                'range' => [
                    self::FIELD_TEXT,
                    self::FIELD_SELECT,
                    self::FIELD_CHECKBOX,
                    self::FIELD_DATE,
                ],
            ],
        ];
    }

    public function attributeLabels(): array {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'display_name' => 'Title',
            'name' => 'Technical name',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[User]].
     */
    public function getUser(): ActiveQuery {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * Gets query for [[FormField]].
     */
    public function getFormFields(): ActiveQuery {
        return $this->hasMany(FormField::class, ['field_id' => 'id']);
    }

    public function getFieldList(): array {
        return [
            static::FIELD_TEXT => 'Text field',
            static::FIELD_SELECT => 'Select',
            static::FIELD_CHECKBOX => 'Checkbox',
            static::FIELD_DATE => 'Date',
        ];
    }

    /**
     * @throws NotFoundHttpException
     */
    public function findModel(int $id): ?Field {
        if (($model = self::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
