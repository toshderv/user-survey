<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "forms".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $name
 * @property int $created_at
 * @property int $updated_at
 * @property boolean $isDeleted
 *
 * @property FormField[] $formFields
 * @property User $user
 * @property UserFormResult[] $userFormResults
 */
class Form extends ActiveRecord {
    public static function tableName(): string {
        return 'forms';
    }

    public function behaviors(): array {
        return [
            TimestampBehavior::class,
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'isDeleted' => true,
                ],
            ],
        ];
    }

    public function rules(): array {
        return [
            [['user_id'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    public function attributeLabels(): array {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Form name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[FormField]].
     */
    public function getFormFields(): ActiveQuery {
        return $this->hasMany(FormField::class, ['form_id' => 'id']);
    }

    /**
     * Gets query for [[User]].
     */
    public function getUser(): ActiveQuery {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * Gets query for [[UserFormResult]].
     */
    public function getUserFormResults(): ActiveQuery {
        return $this->hasMany(UserFormResult::class, ['form_id' => 'id']);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function findModel(int $id): ?Form {
        if (($model = self::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
