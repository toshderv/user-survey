<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserFormResultSearch represents the model behind the search form of `common\models\UserFormResult`.
 */
class UserFormResultSearch extends UserFormResult {
    public function rules(): array {
        return [
            [['id', 'form_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['value'], 'safe'],
        ];
    }

    public function scenarios(): array {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     */
    public function search(array $params, $id = null): ActiveDataProvider {
        if ($id === User::ROLE_ADMIN || is_null($id)) {
            $query = UserFormResult::find();
        } else {
            $query = UserFormResult::find()->where(['=', 'user_id', $id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'form_id' => $this->form_id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'isDeleted' => false,
        ]);

        $query->andFilterWhere(['like', 'value', $this->value]);

        return $dataProvider;
    }
}
