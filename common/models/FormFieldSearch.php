<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * FormFieldSearch represents the model behind the search form of `common\models\FormField`.
 */
class FormFieldSearch extends FormField {
    public function rules(): array {
        return [
            [['id', 'field_id', 'form_id', 'sort', 'created_at', 'updated_at'], 'integer'],
            [['display_name'], 'safe'],
        ];
    }

    public function scenarios(): array {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     */
    public function search(array $params): ActiveDataProvider {
        $query = FormField::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'field_id' => $this->field_id,
            'form_id' => $this->form_id,
            'sort' => $this->sort,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'isDeleted' => false,
        ]);

        $query->andFilterWhere(['like', 'display_name', $this->display_name]);

        return $dataProvider;
    }
}
