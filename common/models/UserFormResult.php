<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "user_form_results".
 *
 * @property int $id
 * @property int|null $form_id
 * @property int|null $user_id
 * @property $value
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Form $form
 * @property User $user
 */
class UserFormResult extends ActiveRecord {
    public static function tableName(): string {
        return 'user_form_results';
    }

    public function behaviors(): array {
        return [
            TimestampBehavior::class,
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'isDeleted' => true,
                ],
            ],
        ];
    }

    /**
     * Finds a date string and formats it.
     * The `value` field encodes in the json
     *
     * @param bool $insert
     */
    public function beforeSave($insert): bool {
        if (parent::beforeSave($insert)) {
            $values = $this->value;

            foreach ($values as $key => $value) {
                // unix timestamp
                $time = strtotime($value);

                if ($time) {
                    $values[$key] = $time;
                }
            }

            $this->value = json_encode($values);

            return true;
        }

        return false;
    }

    public function rules(): array {
        return [
            [['form_id', 'user_id'], 'integer'],
            [['value'], 'required'],
            [['value'], 'safe'],
            [
                ['form_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Form::class,
                'targetAttribute' => ['form_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    public function attributeLabels(): array {
        return [
            'id' => 'ID',
            'form_id' => 'Form ID',
            'user_id' => 'User ID',
            'value' => 'Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Form]].
     */
    public function getForm(): ActiveQuery {
        return $this->hasOne(Form::class, ['id' => 'form_id']);
    }

    /**
     * Gets query for [[User]].
     */
    public function getUser(): ActiveQuery {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
