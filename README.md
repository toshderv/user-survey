<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">User survey</h1>
    <br>
</p>

## About "User survey"

The application makes it possible to administer the elements of the questionnaires, to record the results. Users have the ability to choose profiles, fill out and manage their profiles.

## Install

1. `composer install`
2. `yii help user/create` *Options:* `--user-type="self::USER_ROLE_LIST"`  
***The command will display the accesses to the console***

## How magrations were created

1. `yii migrate/create add_role_column_first_name_column_last_name_column_to_users_table --fields="role:tinyInteger:unsigned:notNull:defaultValue(2):comment('1 - admin; 2 - user'),first_name:string(50):notNull:defaultValue('Firstname'),last_name:string(250):notNull:defaultValue('Lastname')"`
2. `yii migrate/create create_fields_table --fields="user_id:integer:foreignKey(users),display_name:string(255):notNull:comment('title'),name:string(255):notNull:comment('name in the db'),type:tinyInteger:unsigned:notNull:comment('1 - text; 2 - select; 3 - checkbox; 4 - date'),created_at:integer:notNull,updated_at:integer:notNull"`
3. `yii migrate/create create_forms_table --fields="user_id:integer:foreignKey(users),name:string(255),created_at:integer:notNull,updated_at:integer:notNull"`
4. `yii migrate/create create_form_fields_table --fields="field_id:integer:foreignKey(fields),form_id:integer:foreignKey(forms),display_name:string(1000):notNull,sort:integer:notNull:defaultValue(1):comment('sequential number on the form'),created_at:integer:notNull,updated_at:integer:notNull"`
5. `yii migrate/create create_user_form_results_table --fields="form_id:integer:foreignKey(forms),user_id:integer:foreignKey(users),value:json:notNull,created_at:integer:notNull,updated_at:integer:notNull"`
6. `yii migrate/create add_isDeleted_column_to_fields_table --fields="isDeleted:boolean:notNull:defaultValue(false)"`
7. `yii migrate/create add_isDeleted_column_to_forms_table --fields="isDeleted:boolean:notNull:defaultValue(false)"`
8. `yii migrate/create add_isDeleted_column_to_form_fields_table --fields="isDeleted:boolean:notNull:defaultValue(false)"`

DIRECTORY STRUCTURE ADDED/CHANGES
-------------------

```
common
    config/              contains shared configurations
    models/              contains model classes used in both backend and frontend    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations          contains files generated during runtime
backend
    config/              contains backend configurations
    controllers/         contains Web controller classes    
    views/               contains view files for the Web application
frontend
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    views/               contains view files for the Web application
vendor/                  contains dependent 3rd-party packages
```
