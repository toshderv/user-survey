<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%form_fields}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%fields}}`
 * - `{{%forms}}`
 */
class m210707_192255_create_form_fields_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%form_fields}}', [
            'id' => $this->primaryKey(),
            'field_id' => $this->integer(),
            'form_id' => $this->integer(),
            'display_name' => $this->string(1000)->notNull(),
            'sort' => $this->integer()->notNull()->defaultValue(1)->comment('sequential number on the form'),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        // creates index for column `field_id`
        $this->createIndex(
            '{{%idx-form_fields-field_id}}',
            '{{%form_fields}}',
            'field_id'
        );

        // add foreign key for table `{{%fields}}`
        $this->addForeignKey(
            '{{%fk-form_fields-field_id}}',
            '{{%form_fields}}',
            'field_id',
            '{{%fields}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // creates index for column `form_id`
        $this->createIndex(
            '{{%idx-form_fields-form_id}}',
            '{{%form_fields}}',
            'form_id'
        );

        // add foreign key for table `{{%forms}}`
        $this->addForeignKey(
            '{{%fk-form_fields-form_id}}',
            '{{%form_fields}}',
            'form_id',
            '{{%forms}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%fields}}`
        $this->dropForeignKey(
            '{{%fk-form_fields-field_id}}',
            '{{%form_fields}}'
        );

        // drops index for column `field_id`
        $this->dropIndex(
            '{{%idx-form_fields-field_id}}',
            '{{%form_fields}}'
        );

        // drops foreign key for table `{{%forms}}`
        $this->dropForeignKey(
            '{{%fk-form_fields-form_id}}',
            '{{%form_fields}}'
        );

        // drops index for column `form_id`
        $this->dropIndex(
            '{{%idx-form_fields-form_id}}',
            '{{%form_fields}}'
        );

        $this->dropTable('{{%form_fields}}');
    }
}
