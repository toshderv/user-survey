<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%form_fields}}`.
 */
class m210711_104030_add_isDeleted_column_to_form_fields_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%form_fields}}', 'isDeleted', $this->boolean()->notNull()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%form_fields}}', 'isDeleted');
    }
}
