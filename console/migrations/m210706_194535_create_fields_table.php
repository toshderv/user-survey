<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%fields}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%users}}`
 */
class m210706_194535_create_fields_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%fields}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'display_name' => $this->string(255)->notNull()->comment('title'),
            'name' => $this->string(255)->notNull()->comment('name in the db'),
            'type' => $this->tinyInteger()->unsigned()->notNull()->comment('1 - text; 2 - select; 3 - checkbox; 4 - date'),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-fields-user_id}}',
            '{{%fields}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-fields-user_id}}',
            '{{%fields}}',
            'user_id',
            '{{%users}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-fields-user_id}}',
            '{{%fields}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-fields-user_id}}',
            '{{%fields}}'
        );

        $this->dropTable('{{%fields}}');
    }
}
