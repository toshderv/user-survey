<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_form_results}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%forms}}`
 * - `{{%users}}`
 */
class m210707_194115_create_user_form_results_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_form_results}}', [
            'id' => $this->primaryKey(),
            'form_id' => $this->integer(),
            'user_id' => $this->integer(),
            'value' => $this->json()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        // creates index for column `form_id`
        $this->createIndex(
            '{{%idx-user_form_results-form_id}}',
            '{{%user_form_results}}',
            'form_id'
        );

        // add foreign key for table `{{%forms}}`
        $this->addForeignKey(
            '{{%fk-user_form_results-form_id}}',
            '{{%user_form_results}}',
            'form_id',
            '{{%forms}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-user_form_results-user_id}}',
            '{{%user_form_results}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-user_form_results-user_id}}',
            '{{%user_form_results}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%forms}}`
        $this->dropForeignKey(
            '{{%fk-user_form_results-form_id}}',
            '{{%user_form_results}}'
        );

        // drops index for column `form_id`
        $this->dropIndex(
            '{{%idx-user_form_results-form_id}}',
            '{{%user_form_results}}'
        );

        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-user_form_results-user_id}}',
            '{{%user_form_results}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-user_form_results-user_id}}',
            '{{%user_form_results}}'
        );

        $this->dropTable('{{%user_form_results}}');
    }
}
