<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user_form_results}}`.
 */
class m210710_114506_add_isDeleted_column_to_user_form_results_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_form_results}}', 'isDeleted', $this->boolean()->notNull()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_form_results}}', 'isDeleted');
    }
}
