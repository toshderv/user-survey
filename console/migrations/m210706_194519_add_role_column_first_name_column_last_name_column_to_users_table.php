<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%users}}`.
 */
class m210706_194519_add_role_column_first_name_column_last_name_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%users}}', 'role', $this->tinyInteger()->unsigned()->notNull()->defaultValue(2)->comment('1 - admin; 2 - user'));
        $this->addColumn('{{%users}}', 'first_name', $this->string(50)->notNull()->defaultValue('Firstname'));
        $this->addColumn('{{%users}}', 'last_name', $this->string(250)->notNull()->defaultValue('Lastname'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%users}}', 'role');
        $this->dropColumn('{{%users}}', 'first_name');
        $this->dropColumn('{{%users}}', 'last_name');
    }
}
