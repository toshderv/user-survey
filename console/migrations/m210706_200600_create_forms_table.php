<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%forms}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%users}}`
 */
class m210706_200600_create_forms_table extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('{{%forms}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string(255),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-forms-user_id}}',
            '{{%forms}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-forms-user_id}}',
            '{{%forms}}',
            'user_id',
            '{{%users}}',
            'id',
            'SET NULL',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-forms-user_id}}',
            '{{%forms}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-forms-user_id}}',
            '{{%forms}}'
        );

        $this->dropTable('{{%forms}}');
    }
}
