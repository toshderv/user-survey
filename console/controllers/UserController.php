<?php

namespace console\controllers;

use common\models\User;
use Faker\Factory;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\base\InvalidConfigException;
use yii\helpers\Console;

/**
 * Manages users data
 *
 * @package console\controllers
 */
class UserController extends Controller {
    private const USER_PASSWORD = '123456';
    private const USER_ROLE_LIST = [
        'admin' => User::ROLE_ADMIN,
        'user' => User::ROLE_USER,
    ];
    /**
     * @var string the name of the table for keeping applied user information.
     */
    public $userTable = '{{%users}}';
    /**
     * @var string User role. Must be the key of an `self::USER_ROLE_LIST`
     */
    public $userType = 'user';

    /**
     * This method is invoked right before an action is to be executed (after all possible filters.)
     *
     * @param \yii\base\Action $action the action to be executed.
     *
     * @return bool whether the action should continue to be executed.
     * @throws InvalidConfigException if the role specified in userType is not correct.
     */
    public function beforeAction($action) {
        if (!array_key_exists($this->userType, self::USER_ROLE_LIST)) {
            throw new InvalidConfigException(
                'Wrong role. Role must be ['
                . implode(' or ', array_keys(self::USER_ROLE_LIST))
                . '].'
            );
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function options($actionID) {
        return array_merge(
            parent::options($actionID), // global for all actions
            $actionID === 'create' ? ['userType'] : [] // action create
        );
    }

    /**
     * {@inheritdoc}
     */
    public function optionAliases() {
        return ['u' => 'userType'];
    }

    /**
     * Creates a new user.
     *
     * This command creates a new user and displays his access
     * The default role is `user`
     */
    public function actionCreate(): int {
        $user = Yii::$container->get(User::class);
        $faker = Factory::create();
        $userName = $faker->userName;

        $user->username = $userName;
        $user->email = $faker->email;
        $user->status = User::STATUS_ACTIVE;
        $user->role = self::USER_ROLE_LIST[$this->userType];
        $user->first_name = $faker->firstName;
        $user->last_name = $faker->lastName;
        $user->setPassword(self::USER_PASSWORD);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();

        if ($user->save()) {
            $this->stdout("The new user was created successfully.\n", Console::FG_GREEN);
            $this->stdout("Role: ");
            $this->stdout($this->userType . "\n", Console::FG_YELLOW);
            $this->stdout("Username: ");
            $this->stdout($userName . "\n", Console::FG_YELLOW);
            $this->stdout("Password: ");
            $this->stdout(self::USER_PASSWORD . "\n", Console::FG_YELLOW);

            return ExitCode::OK;
        }

        return ExitCode::UNSPECIFIED_ERROR;
    }
}